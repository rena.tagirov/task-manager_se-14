package ru.tagirov.tm.util;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
@RequiredArgsConstructor
public class Md5Util {

    @NotNull
    public static String getHash(final String string) throws NoSuchAlgorithmException {
        @NotNull final MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(string.getBytes());
        byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest).toUpperCase();
    }
}
