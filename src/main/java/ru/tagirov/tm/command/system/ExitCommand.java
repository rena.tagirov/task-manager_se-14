package ru.tagirov.tm.command.system;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;

public class ExitCommand extends AbstractCommand {

    @Override
    public @NonNull String command() {
        return "exit";
    }

    @Override
    public @NonNull String description() {
        return "exit with you account";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException {

        System.out.println("[YOU ARE LOGGED OUT OF ACCOUNT!]");
        serviceLocator.getIUserService().setCurrentUser(null);
        System.out.println("[OK]");
        System.out.println();
    }
}