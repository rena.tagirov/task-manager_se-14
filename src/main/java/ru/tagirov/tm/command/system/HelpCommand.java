package ru.tagirov.tm.command.system;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

public class HelpCommand extends AbstractCommand {

    @Override
    public @NonNull String command() {
        return "help";
    }

    @Override
    public @NonNull String description() {
        return "help";
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() {

        @Nullable final User user = serviceLocator.getIUserService().getCurrentUser();
        for (@NotNull final AbstractCommand command : serviceLocator.getCommands()) {
            if (user == null && !command.isSecure()) {
                System.out.println(new StringBuilder().append(command.command())
                        .append(": ")
                        .append(command.description()));
                continue;
            }
            if (user != null && command.isSecure() && (command.getRole().getLvl() <= user.getRole().getLvl())) {
                System.out.println(new StringBuilder().append(command.command())
                        .append(": ")
                        .append(command.description()));
            }
            if("help".equals(command.command()) || "about".equals(command.command())){
                System.out.println(new StringBuilder().append(command.command())
                        .append(": ")
                        .append(command.description()));
            }
        }
    }
}
