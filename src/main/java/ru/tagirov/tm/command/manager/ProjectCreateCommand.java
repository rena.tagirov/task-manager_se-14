package ru.tagirov.tm.command.manager;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.PurposeStatus;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.util.DateUtil;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class ProjectCreateCommand extends AbstractCommand {

    @Override
    public void setServiceLocator(ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
    }

    @Override
    public @NonNull String command() {
        return "project create";
    }

    @Override
    public @NonNull String description() {
        return "create a new project";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.MANAGER;
    }

    @Override
    public void execute() throws IOException, ParseException, SQLException {

        System.out.println("[PROJECT CREATE]");

        @NotNull final IProjectService projectService = serviceLocator.getIProjectService();
        @NotNull final User user;
        if (serviceLocator.getIUserService().getCurrentUser().getRole() == Role.USER){
            user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());
        }else {
            user = serviceLocator.getTerminalService().getUser();
        }

        System.out.println("ENTER NAME:");
        @NotNull final String name = serviceLocator.getTerminalService().readLine();

        System.out.println("ENTER DESCRIPTION");
        @NotNull final String description = serviceLocator.getTerminalService().readLine();

        System.out.println("START TIME(DD-MM-YYYY):");
        @NotNull final Date beginDate = DateUtil.getDate(serviceLocator.getTerminalService().readLine());

        System.out.println("END TIME(DD-MM-YYYY):");
        @NotNull final Date endDate = DateUtil.getDate(serviceLocator.getTerminalService().readLine());

        @NotNull final Project project = new Project(UUID.randomUUID().toString(), name, description, beginDate, endDate, user.getId(), PurposeStatus.PLANNED);
        projectService.persist(project);
        System.out.println("[OK]");
    }
}
