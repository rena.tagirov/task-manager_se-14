package ru.tagirov.tm.command.manager;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProjectClearCommand extends AbstractCommand {

    @Override
    public void setServiceLocator(ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
    }

    @Override
    public @NonNull String command() {
        return "project clear";
    }

    @Override
    public @NonNull String description() {
        return "delete all projects";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.MANAGER;
    }

    @Override
    public void execute() throws SQLException, IOException {

        @NotNull final IProjectService projectService = serviceLocator.getIProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getITaskService();
        @NotNull final User user;
        if (serviceLocator.getIUserService().getCurrentUser().getRole() == Role.USER){
            user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());
        }else {
            user = serviceLocator.getTerminalService().getUser();
        }

        @NotNull final List<Project> projects = new ArrayList<>(projectService.findAllByUserId(user.getId()));

        for (@NotNull final Project project : projects) {
            taskService.removeAllByProjectId(user.getId(), project.getId());
        }

        projectService.removeAllByUserId(user.getId());
        System.out.println("[ALL PROJECT REMOVE]");
    }
}
