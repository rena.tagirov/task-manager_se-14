package ru.tagirov.tm.command.user;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TaskRemoveCommand extends AbstractCommand {

    @Override
    public @NonNull String command() {
        return "task remove";
    }

    @Override
    public @NonNull String description() {
        return "remove one task";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException, SQLException {

        System.out.println("[TASK REMOVE]");
        @NotNull final ITaskService taskService = serviceLocator.getITaskService();
        @NotNull final User user;
        if (serviceLocator.getIUserService().getCurrentUser().getRole() == Role.USER){
            user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());
        }else {
            user = serviceLocator.getTerminalService().getUser();
        }

        @NotNull final List<Task> tasks;
        if (user.getRole() == Role.USER){
            tasks = new ArrayList<>(taskService.findAllByUserId(user.getId()));
        }else {
            tasks = new ArrayList<>(taskService.findAllByUserId(serviceLocator.getTerminalService().getUser().getId()));
        }

        serviceLocator.getTerminalService().printTasks(tasks);

        System.out.println("ENTER NUMBER:");
        final int number = serviceLocator.getTerminalService().parseInt();

        if(number <= 0 || number > tasks.size()){
            System.out.println("Enter correct number!");
            return;
        }

        for (int i = 0; i < tasks.size(); i++) {
            if ((number - 1) == i) {
                taskService.removeByUserId(user.getId(), tasks.get(i).getId());
            }
        }

        System.out.println("[OK]");
    }
}