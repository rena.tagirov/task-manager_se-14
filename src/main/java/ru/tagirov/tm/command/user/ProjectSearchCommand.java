package ru.tagirov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProjectSearchCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return "project find";
    }

    @Override
    public @NotNull String description() {
        return "Find project by name or description.";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException, SQLException {
        System.out.println("[PROJECT FIND]");

        @NotNull final IProjectService projectService = serviceLocator.getIProjectService();
        @NotNull final User user;
        if (serviceLocator.getIUserService().getCurrentUser().getRole() == Role.USER){
            user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());
        }else {
            user = serviceLocator.getTerminalService().getUser();
        }

        System.out.println("Find:");
        @NotNull final String line = serviceLocator.getTerminalService().readLine();

        @NotNull final List<Project> findProjects = new ArrayList<>(projectService.findAllByUserId(user.getId()));

        for (Project p : findProjects){
            if (p.getName().contains(line) || p.getDescription().contains(line)){
                serviceLocator.getTerminalService().printProject(p);
            }
        }
    }
}
