package ru.tagirov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.enumeration.Role;

import javax.persistence.*;
import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "app_user")
public class User extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 4035276004503441123L;

    @Column(name = "login")
    @NotNull
    private String login;

    @Column(name = "password")
    @NotNull
    private String password;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    @NotNull
    private Role role;

    public User(@NonNull final String id,
                @NonNull final String login,
                @NonNull final String password,
                @NonNull final Role role) {
        super(id);
        this.login = login;
        this.password = password;
        this.role = role;
    }
}
