package ru.tagirov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.enumeration.PurposeStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
@MappedSuperclass
public class PurposeEntity extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 4035276004503442617L;

    @Column(name = "name")
    @NonNull
    private String name;

    @Column(name = "description")
    @NonNull
    private String description;

    @Column(name = "date_create")
    @NonNull
    private Date dateCreate;

    @Column(name = "date_update")
    @Nullable
    private Date dateUpdate;

    @Column(name = "user_id")
    @NonNull
    private String userId;

    @Column(name = "purpose_status")
    @Enumerated(EnumType.STRING)
    @NotNull
    private PurposeStatus purposeStatus;

    public PurposeEntity(@NotNull final String id,
                         @NotNull final String name,
                         @NotNull final String description,
                         @NotNull final Date dateCreate,
                         @NotNull final Date dateUpdate,
                         @NotNull final String userId,
                         @NotNull final PurposeStatus purposeStatus) {
        super(id);
        this.name = name;
        this.description = description;
        this.dateCreate = dateCreate;
        this.dateUpdate = dateUpdate;
        this.userId = userId;
        this.purposeStatus = purposeStatus;
    }
}
