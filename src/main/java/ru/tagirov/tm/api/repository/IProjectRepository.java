package ru.tagirov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.AbstractEntity;
import ru.tagirov.tm.entity.Project;

import java.sql.SQLException;
import java.util.Collection;

public interface IProjectRepository<T extends AbstractEntity> {

    //    CRUD ----------------------------------------------------------------------

    void persist(@NotNull final T t) throws SQLException;

    void merge(@NotNull final T t) throws SQLException;

    @Nullable
    Project findOne(@NotNull final String uuid) throws SQLException;

    @NotNull
    Collection<Project> findAll() throws SQLException;

    void remove(@NotNull final Project project) throws SQLException;

    void removeAll() throws SQLException;

    //    ALL ------------------------------------------------------------------------

    @Nullable
    Project findOneByUserId(@NotNull final String userId, @NotNull final String uuid) throws SQLException;

    @Nullable
    Collection<Project> findAllByUserId(@NotNull final String userId) throws SQLException;

    void removeByUserId(@NotNull final String userId, @NotNull final String uuid) throws SQLException;

    void removeAllByUserId(@NotNull final String userId) throws SQLException;

    @NotNull
    Collection<Project> findAllByLine(final @NotNull String userId, final @NotNull String line) throws SQLException;

}
