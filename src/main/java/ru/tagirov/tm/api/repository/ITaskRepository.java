package ru.tagirov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.AbstractEntity;
import ru.tagirov.tm.entity.Task;

import javax.validation.constraints.NotNull;
import java.sql.SQLException;
import java.util.Collection;

public interface ITaskRepository<T extends AbstractEntity> {

    //    CRUD ----------------------------------------------------------------------

    void persist(@NotNull final Task task) throws SQLException;

    @Nullable
    void merge(@NotNull final Task task) throws SQLException;

    @Nullable
    Task findOne(@NotNull final String uuid) throws SQLException;

    @NotNull
    Collection<Task> findAll() throws SQLException;

    @Nullable
    void remove(@NotNull final Task task) throws SQLException;

    void removeAll() throws SQLException;

    //    ALL ------------------------------------------------------------------------

    @Nullable
    Task findOneByUserId(@NotNull final String userId, @NotNull final String uuid) throws SQLException;

    @NotNull
    Collection<Task> findAllByUserId(@NotNull final String userId) throws SQLException;

    @Nullable
    void removeByUserId(@NotNull final String userId, @NotNull final String uuid) throws SQLException;

    void removeAllByUserId(@NotNull final String userId) throws SQLException;

    @NotNull
    Collection<Task> findAllByLine(final @NotNull String userId, final @NotNull String line) throws SQLException;

    @NotNull
    Collection<Task> findAllByProjectId(@NotNull final String userId, @Nullable final String projectId) throws SQLException;

    void removeAllByProjectId(@NotNull final String userId, @Nullable final String projectId) throws SQLException;


}
