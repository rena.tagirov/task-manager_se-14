package ru.tagirov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.entity.AbstractEntity;
import ru.tagirov.tm.entity.User;

import java.util.Collection;

public interface IUserRepository<T extends AbstractEntity> {

    void persist(@NotNull final T t);

    void merge(@NotNull final T t);

    @NotNull User findOne(@NotNull final String uuid);

    @NotNull Collection<T> findAll();

    void remove(@NotNull final T t);

    void removeAll(@NotNull final T t);
}
