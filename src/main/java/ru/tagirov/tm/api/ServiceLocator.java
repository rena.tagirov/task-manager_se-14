package ru.tagirov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.api.service.IUserService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.terminal.TerminalService;

import javax.persistence.EntityManagerFactory;
import java.util.Collection;

public interface ServiceLocator {

    @NotNull
    IProjectService getIProjectService();

    @NotNull
    ITaskService getITaskService();

    @NotNull
    IUserService getIUserService();

    @NotNull
    Collection<AbstractCommand> getCommands();


    @NotNull
    TerminalService getTerminalService();

    @NotNull
    EntityManagerFactory factory();


}
