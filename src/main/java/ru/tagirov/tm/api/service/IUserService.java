package ru.tagirov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.User;

import java.sql.SQLException;
import java.util.Collection;

public interface IUserService {

//    CRUD ----------------------------------------------------------------------

    void persist(@NotNull final User user) throws SQLException;

    void merge(@NotNull final User user) throws SQLException;

    @Nullable
    User findOne(@NotNull final User user) throws SQLException;

    @NotNull
    Collection<User> findAll() throws SQLException;

    void remove(@NotNull final User user) throws SQLException;

    void removeAll() throws SQLException;

//    ALL ------------------------------------------------------------------------

    @Nullable User getCurrentUser();

     void setCurrentUser(@Nullable User user);

    //    UPDATE -----------------------------------------------------------

    void updateLogin(@NotNull String userId, @NotNull String login);
}
