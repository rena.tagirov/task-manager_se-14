package ru.tagirov.tm;

import ru.tagirov.tm.init.Bootstrap;

import java.io.IOException;

public class Application {

    public static void main( String[] args ) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }
}
