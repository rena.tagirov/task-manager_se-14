package ru.tagirov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.api.repository.IProjectRepository;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.repository.ProjectRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.SQLException;
import java.util.Collection;

@NoArgsConstructor
public class ProjectService implements IProjectService {

    EntityManagerFactory factory;

    public ProjectService(@NotNull final ServiceLocator serviceLocator) {
        this.factory = serviceLocator.factory();
    }

    //    CRUD ----------------------------------------------------------------------

    @Override
    public void persist(@NotNull Project project) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final IProjectRepository<Project> projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.persist(project);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void merge(@NotNull Project project) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final IProjectRepository<Project> projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.merge(project);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public @Nullable Project findOne(@NotNull final Project project) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final IProjectRepository<Project> projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        @NotNull Project newProject = projectRepository.findOne(project.getId());
        entityManager.close();
        return newProject;
    }

    @Override
    public @NotNull Collection<Project> findAll() throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final IProjectRepository<Project> projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        @NotNull Collection<Project> projects= projectRepository.findAll();
        entityManager.close();
        return projects;
    }

    @Override
    public void remove(@NotNull final Project project) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final IProjectRepository<Project> projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.remove(project);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final IProjectRepository<Project> projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    //    ALL ------------------------------------------------------------------------

    @Override
    public @Nullable Project findOneByUserId(@NotNull String userId, @NotNull String uuid) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final IProjectRepository<Project> projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        @NotNull Project newProject = projectRepository.findOneByUserId(userId, uuid);
        entityManager.close();
        return newProject;
    }

    @Override
    public @NotNull Collection<Project> findAllByUserId(@NotNull String userId) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final IProjectRepository<Project> projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        @NotNull Collection<Project> projects= projectRepository.findAllByUserId(userId);
        entityManager.close();
        return projects;
    }

    @Override
    public void removeByUserId(@NotNull String userId, @NotNull String uuid) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final IProjectRepository<Project> projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.removeByUserId(userId, uuid);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAllByUserId(@NotNull String userId) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final IProjectRepository<Project> projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.removeAllByUserId(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public @NotNull Collection<Project> findAllByLine(@NotNull String userId, @NotNull String line) throws SQLException {
        return null;
    }
}

