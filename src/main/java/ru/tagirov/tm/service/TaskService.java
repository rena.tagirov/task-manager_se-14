package ru.tagirov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.api.repository.ITaskRepository;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.SQLException;
import java.util.Collection;

@NoArgsConstructor
public class TaskService implements ITaskService {

    EntityManagerFactory factory;

    public TaskService(@NotNull final ServiceLocator serviceLocator) {
        this.factory = serviceLocator.factory();
    }

    //    CRUD ----------------------------------------------------------------------

    @Override
    public void persist(@NotNull Task task) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final ITaskRepository<Task> taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.persist(task);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void merge(@NotNull Task task) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final ITaskRepository<Task> taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.persist(task);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public @Nullable Task findOne(@NotNull String uuid) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final ITaskRepository<Task> taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        @NotNull Task newTask = taskRepository.findOne(uuid);
        entityManager.close();
        return newTask;    }

    @Override
    public @NotNull Collection<Task> findAll() throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final ITaskRepository<Task> taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        @NotNull Collection<Task> tasks= taskRepository.findAll();
        entityManager.close();
        return tasks;    }

    @Override
    public void remove(@NotNull final Task task) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final ITaskRepository<Task> taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.remove(task);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final ITaskRepository<Task> taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    //    ALL ------------------------------------------------------------------------

    @Override
    public @Nullable Task findOneByUserId(@NotNull String userId, @NotNull String uuid) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final ITaskRepository<Task> taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        @NotNull Task newTask = taskRepository.findOneByUserId(userId, uuid);
        entityManager.close();
        return newTask;    }

    @Override
    public @NotNull Collection<Task> findAllByUserId(@NotNull String userId) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final ITaskRepository<Task> taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        @NotNull Collection<Task> tasks = taskRepository.findAllByUserId(userId);
        entityManager.close();
        return tasks;
    }

    @Override
    public void removeByUserId(@NotNull String userId, @NotNull String uuid) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final ITaskRepository<Task> taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.removeByUserId(userId, uuid);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAllByUserId(@NotNull String userId) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final ITaskRepository<Task> taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.removeAllByUserId(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public @NotNull Collection<Task> findAllByLine(@NotNull String userId, @NotNull String line) throws SQLException {
        return null;
    }

    @Override
    public Collection<Task> findAllByProjectId(String userId, @Nullable String projectId) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final ITaskRepository<Task> taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        @NotNull Collection<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        entityManager.close();
        return tasks;    }

    @Override
    public void removeAllByProjectId(String userId, @Nullable String projectId) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final ITaskRepository<Task> taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.removeAllByProjectId(userId, projectId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}
