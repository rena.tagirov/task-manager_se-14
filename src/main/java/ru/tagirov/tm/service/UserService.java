package ru.tagirov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.api.repository.IUserRepository;
import ru.tagirov.tm.api.service.IUserService;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.repository.UserRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.SQLException;
import java.util.Collection;

@NoArgsConstructor
public class UserService implements IUserService {

    EntityManagerFactory factory;

    public UserService(@NotNull final ServiceLocator serviceLocator) {
        this.factory = serviceLocator.factory();
    }

    //    CRUD ----------------------------------------------------------------------

    @Override
    public void persist(@NotNull User user) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final IUserRepository<User> userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.persist(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void merge(@NotNull User user) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final IUserRepository<User> userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.merge(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public @Nullable User findOne(@NotNull final User user) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final IUserRepository<User> userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        @NotNull User newUser = userRepository.findOne(user.getId());
        entityManager.close();
        return newUser;

    }

    @Override
    public @NotNull Collection<User> findAll() throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final IUserRepository<User> userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        @NotNull Collection<User> users= userRepository.findAll();
        entityManager.close();
        return users;
    }

    @Override
    public void remove(@NotNull final User user) throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final IUserRepository<User> userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.remove(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final EntityManager entityManager = factory.createEntityManager();
        final IUserRepository<User> userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.removeAll(user);
        entityManager.close();
    }

    //    ALL ------------------------------------------------------------------------

    @Nullable
    private User user = null;

    @Override
    public @Nullable User getCurrentUser() {
        return user;
    }

    @Override
    public void setCurrentUser(@Nullable User user) {
        this.user = user;
    }

    //    UPDATE -----------------------------------------------------------

    @Override
    public void updateLogin(@NotNull String userId, @NotNull String login) {

    }
}
