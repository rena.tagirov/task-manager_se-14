package ru.tagirov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.api.repository.IProjectRepository;
import ru.tagirov.tm.entity.Project;

import javax.persistence.EntityManager;
import java.sql.SQLException;
import java.util.Collection;

public class ProjectRepository implements IProjectRepository<Project> {

    @NotNull
    private EntityManager entityManager;

    public ProjectRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    //    CRUD ----------------------------------------------------------------------

    @Override
    public void persist(@NotNull Project project) throws SQLException {
        entityManager.persist(project);
    }

    @Override
    public void merge(@NotNull Project project) throws SQLException {
        entityManager.merge(project);
    }

    @Override
    public @Nullable Project findOne(@NotNull String uuid) throws SQLException {
        return entityManager.find(Project.class, uuid);
    }

    @Override
    public @NotNull Collection<Project> findAll() throws SQLException {
        return entityManager.createQuery("SELECT u FROM Project as u", Project.class).getResultList();
    }

    @Override
    public void remove(@NotNull final Project project) throws SQLException {
        @NotNull final Project newProject = entityManager.find(Project.class, project.getId());
        entityManager.remove(newProject);
    }

    @Override
    public void removeAll() throws SQLException {
        entityManager.createQuery("DELETE FROM Project p", Project.class).executeUpdate();
    }

    //    ALL ------------------------------------------------------------------------

    @Override
    public @Nullable Project findOneByUserId(@NotNull String userId, @NotNull String uuid) throws SQLException {
        return entityManager.createQuery("SELECT p FROM Project p WHERE p.user_id =: user_id AND p.id = : id", Project.class).
                setParameter("user_id", userId).
                setParameter("id", uuid).getSingleResult();
    }

    @Override
    public @Nullable Collection<Project> findAllByUserId(@NotNull String userId) throws SQLException {
        return entityManager.createQuery(
                "SELECT p FROM Project p WHERE p.userId = :user_id", Project.class).
                setParameter("user_id",userId).
                getResultList();
    }

    @Override
    public void removeByUserId(@NotNull String userId, @NotNull String uuid) throws SQLException {
        entityManager.createQuery("DELETE FROM Project p WHERE p.user_id = :user_Id AND p.id = : id", Project.class).
                setParameter("user_id",userId).
                setParameter("id", uuid).executeUpdate();
    }

    @Override
    public void removeAllByUserId(@NotNull String userId) throws SQLException {
        entityManager.createQuery("DELETE FROM Project p WHERE p.userId = :user_id", Project.class).
                setParameter("user_id",userId).executeUpdate();
    }

    @Override
    public @NotNull Collection<Project> findAllByLine(@NotNull String userId, @NotNull String line) throws SQLException {
        return null;
    }
}
