package ru.tagirov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.repository.IUserRepository;
import ru.tagirov.tm.entity.User;

import javax.persistence.EntityManager;
import java.util.Collection;

public class UserRepository implements IUserRepository<User> {

    @NotNull
    private EntityManager entityManager;

    public UserRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    //    CRUD ----------------------------------------------------------------------

    @Override
    public void persist(@NotNull User user) {
        entityManager.persist(user);
    }

    @Override
    public void merge(@NotNull User user) {
        entityManager.merge(user);
    }

    @Override
    public @NotNull User findOne(@NotNull String uuid) {
        return entityManager.find(User.class, uuid);
    }

    @Override
    public @NotNull Collection<User> findAll() {
        return entityManager.createQuery("SELECT u FROM User as u", User.class).getResultList();
    }

    @Override
    public void remove(@NotNull User user) {
        @NotNull final User newUser = entityManager.find(User.class, user.getId());
        entityManager.remove(newUser);
    }

    @Override
    public void removeAll(@NotNull User user) {
        entityManager.createQuery("DELETE FROM app_user").executeUpdate();
    }
}
